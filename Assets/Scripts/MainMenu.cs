﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour 
{
    [SerializeField] GameObject _myCanvas;

    private void Start ()
	{
        AppManager.Instance.OnStateChange += ChangeState;
	}

    private void ChangeState (AppManager.ApplicationState state)
    {
        switch (state)
        {
            case AppManager.ApplicationState.Menu:
                ToMenu();
                break;
            case AppManager.ApplicationState.Setup:
            case AppManager.ApplicationState.Presentation:
                ToPresOrSetup();
                break;
            default:
                break;
        }
    }

    private void ToMenu()
    {
        _myCanvas.SetActive(true);
    }

    private void ToPresOrSetup()
    {
        _myCanvas.SetActive(false);
    }
}