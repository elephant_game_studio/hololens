﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArbuzzTools
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T _instance;
        public static T Instance
        {
            get { return _instance; }
            private set { _instance = value; }
        }

        protected virtual void Awake ()
        {
            if (Instance != null)
            {
                Destroy(this);
            }
            else
            {
                Instance = (T)this;
            }
        }
    }
}
