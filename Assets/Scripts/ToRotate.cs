﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.SpatialMapping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HoloFocuse))]
public class ToRotate : MonoBehaviour, INavigationHandler
{
    [SerializeField] private Material _navMaterial;

    HoloFocuse _myHoloNav;

    public void Start ()
	{
        _myHoloNav = GetComponent<HoloFocuse>();
    }

    #region Rotate
    public void OnNavigationStarted (NavigationEventData eventData)
    {
        StartNavigattion();
    }

    private void StartNavigattion ()
    {
        _myHoloNav.ChangeMaterialToAndSave(_navMaterial);
        _myHoloNav.RemoveWorldAnchor();
    }

    public void OnNavigationUpdated (NavigationEventData eventData)
    {
        CheckNavigationMaterial();
        RotateX(eventData.CumulativeDelta.x);
    }

    private void CheckNavigationMaterial ()
    {
        if (_myHoloNav.MyMeshRenderer.material != _navMaterial)
        {
            _myHoloNav.ChangeMaterialToAndSave(_navMaterial);
        }
    }

    private void RotateX (float rotation)
    {
        float rotationFactor = rotation * 2f;
        _myHoloNav.Model.Rotate(new Vector3(0f, -1 * rotationFactor, 0f));
    }

    public void OnNavigationCompleted (NavigationEventData eventData)
    {
        _myHoloNav.BackToPreviousMaterial();
        _myHoloNav.AddWorldAnchor();
    }

    public void OnNavigationCanceled (NavigationEventData eventData)
    {
        _myHoloNav.BackToPreviousMaterial();
    }
    #endregion
}