﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;



public class HoloFocuse : MonoBehaviour, IFocusable
{
    [SerializeField] string _myWolrdAnchorName; 
    [SerializeField, Tooltip ("set if model is not child of this object")] Transform _model;
    
    Material _previousMaterial;
    Material _default;
    
    [SerializeField] private MeshRenderer _myMeshRenderer;
    [SerializeField] private Material _onLookMaterial;
    [SerializeField] private Material _onNotLookMaterial;    

    public MeshRenderer MyMeshRenderer { get { return _myMeshRenderer; } }
    public Material OnLookMaterial { get { return _onLookMaterial; } }
    public Material OnNotLookMaterial { get { return _onNotLookMaterial; } }
    public Transform Model
    {
        get
        {
            if (_model != null) { return _model; }
            else { return transform; } 
        }
    }

    protected WorldAnchorManager _anchorManager;
    protected SpatialMappingManager _spatialMappingManager;

    

    public LayerMask SpatailLayerMask { get { return _spatialMappingManager.LayerMask; } }

    public void Start ()
    {   
        _anchorManager = WorldAnchorManager.Instance;
        if (_anchorManager == null)
        {
            Debug.LogError("This script expects that you have a WorldAnchorManager component in your scene.", gameObject);
        }

        _spatialMappingManager = SpatialMappingManager.Instance;
        if (_spatialMappingManager == null)
        {
            Debug.LogError("This script expects that you have a SpatialMappingManager component in your scene.");
        }

        if (_anchorManager != null && _spatialMappingManager != null)
        {
            AddWorldAnchor();
        }

        _default = _myMeshRenderer.material;
    }

    private void LateUpdate ()
    {
        if (Model != transform)
        {
            transform.position = Model.position;
            transform.rotation = Model.rotation;
        }
    }

    private void OnDisable ()
    {
        _anchorManager.AttachAnchor(Model.gameObject, _myWolrdAnchorName);
    }

    #region Common
    public void RemoveWorldAnchor ()
    {
        _anchorManager.RemoveAnchor(Model.gameObject);
    }

    public void AddWorldAnchor ()
    {
        _anchorManager.AttachAnchor(Model.gameObject, _myWolrdAnchorName);
    }

    public void BackToPreviousMaterial ()
    {
        _myMeshRenderer.material = _previousMaterial;
    }

    public void ChangeMaterialToAndSave (Material toSet)
    {
        _previousMaterial = _myMeshRenderer.material;
        _myMeshRenderer.material = toSet;
    }

    public void SetToDefaultMaterial()
    {
        _myMeshRenderer.material = _default;
    }

    public void SetSpatialVisualize(bool set)
    {
        _spatialMappingManager.DrawVisualMeshes = set;
    }
    #endregion
    
    #region Focus
    public void OnFocusEnter ()
    {
        ChangeMaterialToAndSave(_onLookMaterial);
    }

    public void OnFocusExit ()
    {
        SetToDefaultMaterial();
    }
    #endregion

    #region Rotate
    //public void OnNavigationStarted (NavigationEventData eventData)
    //{
    //    StartNavigattion();
    //}

    //private void StartNavigattion ()
    //{
    //    ChangeMaterialToAndSave(_navMaterial);
    //    RemoveWorldAnchor();
    //}

    //public void OnNavigationUpdated (NavigationEventData eventData)
    //{
    //    CheckNavigationMaterial();
    //    RotateX(eventData.CumulativeDelta.x);
    //}

    //private void CheckNavigationMaterial ()
    //{
    //    if (_myMeshRenderer.material != _navMaterial)
    //    {
    //        _previousMaterial = _myMeshRenderer.material;
    //        _myMeshRenderer.material = _navMaterial;
    //    }
    //}

    //private void RotateX (float rotation)
    //{
    //    float rotationFactor = rotation * 2f;
    //    transform.Rotate(new Vector3(0f, -1 * rotationFactor, 0f));
    //}

    //public void OnNavigationCompleted (NavigationEventData eventData)
    //{
    //    BackToPreviousMaterial();
    //    AddWorldAnchor();
    //}    

    //public void OnNavigationCanceled (NavigationEventData eventData)
    //{
    //    BackToPreviousMaterial();
    //}    
    #endregion

    #region Move
    //private bool _isBeingPlaced;


    //public void OnInputClicked (InputEventData eventData)
    //{
    //    _isBeingPlaced = !_isBeingPlaced;

    //    // If the user is in placing mode, display the spatial mapping mesh.
    //    if (_isBeingPlaced)
    //    {
    //        _spatialMappingManager.DrawVisualMeshes = true;

    //        Debug.Log(gameObject.name + " : Removing existing world anchor if any.");
    //        RemoveWorldAnchor();
    //        _anchorManager.RemoveAnchor(gameObject);
    //    }
    //    // If the user is not in placing mode, hide the spatial mapping mesh.
    //    else
    //    {
    //        _spatialMappingManager.DrawVisualMeshes = true;
    //        // Add world anchor when object placement is done.
    //        AddWorldAnchor();
    //    }
    //}



    //private void TryPlace ()
    //{
    //    if (_isBeingPlaced)
    //    {
    //        RaycastHit hitInfo;
    //        if (RayCastAgainstSpatialMap(out hitInfo))
    //        {
    //            Quaternion toQuat = Camera.main.transform.localRotation;
    //            toQuat.x = 0;
    //            toQuat.z = 0;

    //            gameObject.transform.position = hitInfo.point;
    //            gameObject.transform.rotation = toQuat;
    //        }
    //    }
    //}

    //private bool RayCastAgainstSpatialMap(out RaycastHit hitInfo)
    //{
    //    Vector3 headPosition = Camera.main.transform.position;
    //    Vector3 gazeDirection = Camera.main.transform.forward;

    //    return Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f, _spatialMappingManager.LayerMask);
    //}
    #endregion

}
