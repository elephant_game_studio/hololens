﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;

public class ToLocateInWorld : MonoBehaviour 
{
    [SerializeField] string _myAnchorName;

	private void Start ()
	{
        WorldAnchorManager.Instance.AttachAnchor(gameObject, _myAnchorName);
	}
	
}