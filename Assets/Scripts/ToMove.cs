﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;

[RequireComponent(typeof(HoloFocuse))]
public class ToMove : MonoBehaviour, IInputClickHandler
{

    private HoloFocuse _myHoloNav;
    [SerializeField] private bool _faceWhileMoving;

    public void Start ()
    {
        _myHoloNav = GetComponent<HoloFocuse>();
    }

    protected void Update ()
    {
        TryPlace();
    }

    private bool _isBeingPlaced;


    public void OnInputClicked (InputEventData eventData)
    {
        _isBeingPlaced = !_isBeingPlaced;
        
        if (_isBeingPlaced)
        {
            _myHoloNav.SetSpatialVisualize(true);
            _myHoloNav.RemoveWorldAnchor();
        }
        else
        {
#if UNITY_HOLOLENS && !UNITY_EDITOR
            _myHoloNav.SetSpatialVisualize(false);
#else
            _myHoloNav.SetSpatialVisualize(true);
#endif
            _myHoloNav.AddWorldAnchor();
        }
    }



    private void TryPlace ()
    {
        if (_isBeingPlaced)
        {
            RaycastHit hitInfo;
            if (RayCastAgainstSpatialMap(out hitInfo))
            {
                if (_faceWhileMoving)
                {
                    Quaternion toQuat = Camera.main.transform.localRotation;
                    toQuat.x = 0;
                    toQuat.z = 0;
                    _myHoloNav.Model.rotation = toQuat;
                }

                _myHoloNav.Model.position = hitInfo.point;
                
            }
        }
    }

    private bool RayCastAgainstSpatialMap (out RaycastHit hitInfo)
    {
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;

        return Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f, _myHoloNav.SpatailLayerMask);
    }
}