﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ArbuzzTools;

public class AppManager : Singleton<AppManager> 
{
    public enum ApplicationState { Menu, Setup, Presentation}
    private ApplicationState _state;

    public ApplicationState State
    {
        get { return _state; }
        private set
        {
            _state = value;
        }
    }

    public Action<ApplicationState> OnStateChange;  

    protected override void Awake ()
    {
        base.Awake();
    }

	public void Start ()
	{
        _state = ApplicationState.Menu;
	}

    public void ChangeToSetup()
    {
        State = ApplicationState.Setup;
        FireOnStateChange();
    }

    public void ChangeToPresentation()
    {
        Debug.Log("pres app");
        State = ApplicationState.Presentation;
        FireOnStateChange();
    }

    public void ChangeToMenu ()
    {
        State = ApplicationState.Presentation;
        FireOnStateChange();
    }

    private void FireOnStateChange()
    {
        if (OnStateChange != null)
        {
            Debug.Log("event" + State);
            OnStateChange(State);
        }
    }

    void Update ()
	{
		
	}
}