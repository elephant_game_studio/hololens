﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuickUI : MonoBehaviour 
{
	public void LoadSetup()
    {
        SceneManager.LoadScene(2);
    }

    public void LoadPresentation()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }
}