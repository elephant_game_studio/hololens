﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour 
{
    [SerializeField] Model _model;
    [SerializeField] GameObject _mover;

    private void Start ()
    {
        if (AppManager.Instance != null)
        {
            AppManager.Instance.OnStateChange += ChangeToState;
        }
        _model.gameObject.SetActive(false);
        _mover.SetActive(false);

    }

    private void ChangeToState (AppManager.ApplicationState state)
    {
        Debug.Log(state);
        switch (state)
        {
            case AppManager.ApplicationState.Menu:
                ToMenu();
                break;
            case AppManager.ApplicationState.Setup:
                ToSetup();
                break;
            case AppManager.ApplicationState.Presentation:
                ToPresentation();
                break;
            default:
                break;
        }
    }

    private void ToMenu()
    {
        _model.gameObject.SetActive(false);
        _mover.SetActive(false);
    }

    private void ToPresentation()
    {
        _mover.SetActive(false);
        _model.SetActiveUI(false);
        _model.gameObject.SetActive(true);
    }

    private void ToSetup()
    {
        _mover.SetActive(true);
        _model.gameObject.SetActive(true);
        _model.SetActiveUI(true);
    }

	void Update ()
	{
		
	}
}