﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.VR.WSA;

public class Cube : MonoBehaviour, IFocusable
{
    [SerializeField] Material _onLookMaterial;
    [SerializeField] Material _onNotLookMaterial;
    [SerializeField] Material _navMaterial;

    Material previousMaterial;

    [SerializeField] MeshRenderer _myMeshRenderer;
    

    public void Awake ()
    {
        Debug.Assert(_onNotLookMaterial != null);
        Debug.Assert(_onLookMaterial != null);
        Debug.Assert(_myMeshRenderer != null);
        
    }

    public void OnFocusEnter ()
    {
        _myMeshRenderer.material = _onLookMaterial;
    }

    public void OnFocusExit ()
    {
        _myMeshRenderer.material = _onNotLookMaterial;

    }    
}