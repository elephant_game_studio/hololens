﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceUser : MonoBehaviour 
{	
	void Update ()
	{
        transform.LookAt(Camera.main.transform.position, Vector3.up);
        transform.Rotate(Vector3.up * 180f);
	}
}